
package main

import (
	"os"
	"fmt"
	"log"
	"bytes"
	"strings"
	"strconv"
	"net/http"
	"io/ioutil"
	"crypto/md5"
	"encoding/json"

	"github.com/PuerkitoBio/goquery"
	"github.com/julienschmidt/httprouter"
	"github.com/BurntSushi/toml"
	"github.com/hoisie/mustache"
)

type Action int

const (
	ActionStart Action = iota
	ActionGenerateIndex
	ActionGeneratePost
	ActionGeneratePosts
	ActionGenerateAll
)

type Context struct{
	Conf	Config
	Post	Document
}

type Page struct{
	N	int
	Link	string
	Current	bool
}

func (p Page) Cannon() int {
	return p.N + 1
}

type ContextIndex struct{
	Conf	Config
	Posts	[]Document
	Pages	[]Page
	Page	int
	NPages	int
	Prev	string
	Next	string
}

func (c ContextIndex) HasPrev() bool {
	return c.Page != 0
}

func (c ContextIndex) HasNext() bool {
	return c.Page != c.NPages
}

func (c ContextIndex) PageCannon() int {
	return c.Page + 1
}

type Config struct{
	Id		string
	Path		string
	Listen		string
	PostPerPage	int
	NWords		int
	Users	[]User	`toml:"User"`

	Directories	struct{
		HTML	string
		Data	string
	}
	Templates	struct{
		Index	string
		Post	string
	}

	Misc	map[string]interface{}
}

type User struct{
	Id		int	`json:"id"`
	Email		string	`json:"email"`
	Md5Email	string	`json:"-"`
	Name		string
	Misc		map[string]interface{}
}

type Document struct{
	Id		int	`json:"id"`
	Name		string	`json:"name"`
	Content		string	`json:"content"`
	ContentHtml	string	`json:"content_html"`
	ContentText	string	`json:"-"`
	Short		string	`json:"-"`
	User		User	`json:"user"`
}

type Index struct{
	Posts	[]int
}

func ParseConfig(filename string) (Config, error) {
	var conf Config

	conf.PostPerPage = 4 // default value
	file, err := os.Open(filename)
	if err != nil {
		return conf, err
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return conf, err
	}
	_, err = toml.Decode(string(data), &conf)
	if err != nil {
		return conf, err
	}
	return conf, nil
}

func FindPost(doc Document, conf Config) bool {
	file, err := os.Open(fmt.Sprintf("%s/%d.json", conf.Directories.Data, doc.Id))
	if err != nil {
		return false
	}
	_ = file.Close()
	return true
}

func ReadIndex(conf Config) Index {
	var index Index
	data, err := ioutil.ReadFile(fmt.Sprintf("%s/index.json", conf.Directories.Data))
	if err != nil {
		return Index{make([]int, 0)}
	}
	err = json.Unmarshal(data, &index)
	if err != nil {
		return Index{make([]int, 0)}
	}
	return index
}

func UpdateIndex(id int, conf Config) error {
	index := ReadIndex(conf)
	newPosts := make([]int, len(index.Posts)+1)
	for k, v := range(index.Posts) {
		newPosts[k] = v
	}
	newPosts[len(newPosts)-1] = id
	index.Posts = newPosts
	data, err := json.Marshal(index)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(fmt.Sprintf("%s/index.json", conf.Directories.Data), data, 0640)
	return err
}

func UpdatePost(doc Document, conf Config) error {
	filename := fmt.Sprintf("%s/%d.json", conf.Directories.Data, doc.Id)
	data, err := json.Marshal(doc)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0640)
	if err != nil {
		return err
	}
	return nil
}

func CreatePost(doc Document, conf Config) error {
	filename := fmt.Sprintf("%s/%d.json", conf.Directories.Data, doc.Id)
	data, err := json.Marshal(doc)
	if err != nil {
		return err
	}
	err = ioutil.WriteFile(filename, data, 0640)
	if err != nil {
		return err
	}
	err = UpdateIndex(doc.Id, conf)
	if err != nil {
		return err
	}
	return nil
}

func GenerateHTML(doc Document, conf Config) error {
	context := Context{
		Conf: conf,
		Post: doc,
	}
	context.Post.User.Md5Email = fmt.Sprintf("%x", md5.Sum([]byte(doc.User.Email)))
	data := mustache.RenderFile(conf.Templates.Post, context)
	filename := fmt.Sprintf("%s/%d.html", conf.Directories.HTML, doc.Id)
	err := ioutil.WriteFile(filename, []byte(data), 0640)
	return err
}

func GenerateShort(conf Config, doc *Document) {
	d, err := goquery.NewDocumentFromReader(bytes.NewBufferString(doc.ContentHtml))
	if err != nil {
		return
	}
	s := d.Find("p")
	t := s.Text()
	t = strings.Replace(t, "\t", " ", -1)
	t = strings.Replace(t, "\n", " ", -1)
	t = strings.Replace(t, "\r", " ", -1)
	ts := strings.Split(t, " ")
	t = ""
	short := ""
	j := 0
	for i := range(ts) {
		if len(ts[i]) == 0 {
			continue
		}
		if len(t) == 0 {
			t = ts[i]
			short = ts[i]
		} else {
			t += " " + ts[i]
			if j < conf.NWords {
				short += " " + ts[i]
			}
		}
		j++
	}
	doc.ContentText = t
	doc.Short = short
}

func ReadPost(id int, conf Config) (Document, error) {
	var doc Document
	data, err := ioutil.ReadFile(fmt.Sprintf("%s/%d.json", conf.Directories.Data, id))
	if err != nil {
		return doc, err
	}
	err = json.Unmarshal(data, &doc)
	if err != nil {
		return doc, err
	}
	GenerateShort(conf, &doc)
	for _, u := range(conf.Users) {
		if u.Email == doc.User.Email {
			doc.User = u
		}
	}
	return doc, nil
}

func GeneratePosts(conf Config) error {
	index := ReadIndex(conf)
	for _, id := range(index.Posts) {
		doc, err := ReadPost(id, conf)
		if err != nil {
			return err
		}
		err = GenerateHTML(doc, conf)
		if err != nil {
			return err
		}
	}
	return nil
}

func GenerateIndex(conf Config) error {
	index := ReadIndex(conf)
	nPages := len(index.Posts) / conf.PostPerPage
	if (len(index.Posts) % conf.PostPerPage) != 0 {
		nPages += 1
	}
	for i := 0; i < nPages; i++ {
		GenerateIndexPage(conf, index, i, nPages)
	}
	return nil
}

func GenerateIndexPage(conf Config, index Index, pageNo int, nPages int) error {
	var filename string
	if pageNo == 0 {
		filename = fmt.Sprintf("%s/index.html", conf.Directories.HTML)
	} else {
		filename = fmt.Sprintf("%s/page_%d.html", conf.Directories.HTML, pageNo)
	}
	// index := ReadIndex(conf)
	first := pageNo * conf.PostPerPage
	last := (pageNo + 1) * conf.PostPerPage - 1
	if last >= len(index.Posts) {
		last = len(index.Posts) - 1
	}
	context := ContextIndex{
		Conf: conf,
		Posts: make([]Document, last - first + 1),
		Page: pageNo,
		NPages: nPages,
		Pages: make([]Page, nPages),
	}

	for i := 0; i < nPages; i++ {
		context.Pages[i].N = i
		if i == 0 {
			context.Pages[i].Link = conf.Path + "index.html"
		} else {
			context.Pages[i].Link = fmt.Sprintf("%spage_%d.html", conf.Path, i)
		}
		if i == pageNo {
			context.Pages[i].Current = true
		} else {
			context.Pages[i].Current = false
		}
	}
	j := 0
	for i := first; i <= last; i++ {
		doc, err := ReadPost(index.Posts[i], conf)
		if err != nil {
			return err
		}
		context.Posts[j] = doc
		j++
	}
	if pageNo > 1 {
		context.Prev = fmt.Sprintf("%spage_%d.html", conf.Path, pageNo - 1)
	} else if pageNo == 1 {
		context.Prev = conf.Path + "index.html"
	}
	if pageNo < nPages - 1 {
		context.Next = fmt.Sprintf("%spage_%d.html", conf.Path, pageNo + 1)
	}
	data := mustache.RenderFile(conf.Templates.Index, context)
	err := ioutil.WriteFile(filename, []byte(data), 0640)
	return err
}

func Controller(w http.ResponseWriter, r *http.Request, _ httprouter.Params, conf Config) {
	var doc Document

	err := r.ParseForm()
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("error: couldn't parse form"))
		log.Println(err)
		return
	}
	data := r.PostFormValue("payload")
	err = json.Unmarshal([]byte(data), &doc)
	if err != nil {
		w.WriteHeader(400)
		w.Write([]byte("error: couldn't parse json"))
		log.Println(err)
		return
	}
	if FindPost(doc, conf) {
		// updating post
		err = UpdatePost(doc, conf)
		if err != nil {
			log.Println(err)
			return
		}
		doc, err = ReadPost(doc.Id, conf)
		if err != nil {
			log.Println(err)
			return
		}
		err = GenerateHTML(doc, conf)
		if err != nil {
			log.Println(err)
			return
		}
		err = GenerateIndex(conf)
		if err != nil {
			log.Println(err)
			return
		}
	} else {
		err = CreatePost(doc, conf)
		if err != nil {
			log.Println(err)
			return
		}
		doc, err = ReadPost(doc.Id, conf)
		if err != nil {
			log.Println(err)
			return
		}
		err = GenerateHTML(doc, conf)
		if err != nil {
			log.Println(err)
			return
		}
		err = GenerateIndex(conf)
		if err != nil {
			log.Println(err)
			return
		}
	}
}

func ParseArgs() (Action, int) {
	if len(os.Args) > 1 {
		if os.Args[1] == "start" {
			return ActionStart, 0
		} else if os.Args[1] == "generate" {
			if len(os.Args) > 2 {
				if os.Args[2] == "index" {
					return ActionGenerateIndex, 0
				} else if os.Args[2] == "posts" {
					return ActionGeneratePosts, 0
				} else {
					postId, err := strconv.ParseInt(os.Args[2], 10, 32)
					if err != nil {
						log.Fatal(err)
						return ActionStart, 0
					}
					return ActionGeneratePost, int(postId)
				}
			} else {
				return ActionGenerateAll, 0
			}
		} else {
			fmt.Println("Usage: ", os.Args[0], "<action>")
			fmt.Println("Actions: ")
			fmt.Println("\tstart")
			fmt.Println("\tgenerate # re-generate index and every posts")
			fmt.Println("\tgenerate index")
			fmt.Println("\tgenerate posts")
			fmt.Println("\tgenerate <postId>")
			fmt.Println("")
			os.Exit(2)
		}
	}
	return ActionStart, 0
}

func main() {
	var conf Config
	var err error

	action, postId := ParseArgs()
	conf.Path = "/"
	conf.Listen = ":8080"
	filename := "yblog.toml"
	if os.Getenv("CONF_NAME") != "" {
		filename = os.Getenv("CONF_NAME")
	}
	conf, err = ParseConfig(filename)
	if err != nil {
		log.Fatal(err)
	}
	if action == ActionGenerateAll {
		err = GenerateIndex(conf)
		if err != nil {
			log.Fatal(err)
		}
		err = GeneratePosts(conf)
		if err != nil {
			log.Fatal(err)
		}
		return
	} else if action == ActionGenerateIndex {
		err = GenerateIndex(conf)
		if err != nil {
			log.Fatal(err)
		}
		return
	} else if action == ActionGeneratePosts {
		err = GeneratePosts(conf)
		if err != nil {
			log.Fatal(err)
		}
		return
	} else if action == ActionGeneratePost {
		doc, err := ReadPost(postId, conf)
		if err != nil {
			log.Fatal(err)
		}
		err = GenerateHTML(doc, conf)
		if err != nil {
			log.Fatal(err)
		}
		return
	}
	router := httprouter.New()
	router.POST(conf.Path + conf.Id, func (w http.ResponseWriter, r *http.Request, p httprouter.Params) {
		Controller(w, r, p, conf)
	})

	fmt.Println("listening on", conf.Listen)
	log.Fatal(http.ListenAndServe(conf.Listen, router))
}

